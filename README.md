# REST-сервис для просмотра ЗП

**Для корректной работы необходимо установить:**
* ___[PostgreSQL](https://www.postgresql.org/)___

**Если захотите развернуть приложение в Docker, необходимо будет установить:**
* ___[Docker](https://docs.docker.com/engine/install/)___,
* ___[Docker Compose](https://docs.docker.com/compose/install/)___

Для локального тестирования необходимо создать виртуальное окружение командой `python3 -m venv venv` и активировать его. Команда `venv\Scripts\activate.bat` - для Windows; `source venv/bin/activate` - для Linux и MacOS.

Затем необходимо перейти в папку с нужным уроком и установить зависимости командой `pip install -r requirements.txt`.

На тот случай, если Вы захотите использовать ___[poetry](https://python-poetry.org/docs/ "Документация")___, то Вам будет необходимо в директории с проектом прописать `poetry env use python3.10` для создания виртуального окружения, затем подключиться к нему командой `poetry shell`.

После этого необходимо установить зависимости командой `poetry install`

Затем необходимо перейти в папку **src** командой `cd src` и запустить команду `uvicorn main:app --reload` для запуска сервера uvicorn.

После этого можно зайти в браузере по адресу `http://localhost:8000/docs` для просмотра доступных эндпоинтов.

**Список эндпоинтов:**
* POST /auth/login - используется для входа в учетную запись(в поле **username** необходимо ввести email, указанный при регистрации)
* POST /auth/logout - используется для выхода и учетную запись
* POST /auth/register - иcпользуется для регистрации пользователя
* GET /salary/operations - необходим для просмотра ЗП у текущего пользователя. ЗП и дата повышения установленна по умолчанию и не хранятся в БД. Для того чтобы ЗП внести в БД, необходимо использовать соответствующий POST запрос, который может делать лишь админ
* PUT /salary/operations - необходим для изменения ЗП. Изменять может только админ
* POST /salary/operations - необходим для создания ЗП. Создавать может только админ

Для того чтобы стать админом необходимо в PostgreSQL передать команду `UPDATE "user" SET is_superuser = true WHERE id = num;`, где **num** - *id* нужного пользователя.


Для поднятия Docker необходимо в папке с проектом открыть командую строку и поочередно прописать ``docker build`` ``docker compose build`` ``docker compose up``.

P.S. По каким-то неведомым для меня причинам при попытке войти через развернутый контейнер, не сохраняются куки в браузере и поэтому нельзя проделать операции, которые требуют аутентификации. **При обычном локальном запуске полет нормалый.**
