from datetime import date

from pydantic import BaseModel


class SalaryCreate(BaseModel):

    salary: int
    promotion_date: date
    user_id: int


class SalaryUpdate(BaseModel):
    salary: int
    promotion_date: date
    user_id: int
