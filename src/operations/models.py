from sqlalchemy import Column, Integer, ForeignKey, DATE
from sqlalchemy.orm import relationship

from database import Base


class Salary(Base):
    __tablename__ = "salary"

    id = Column(Integer, primary_key=True,)
    user_id = Column(Integer,
                     ForeignKey("user.id", ondelete="CASCADE"),
                     unique=True)
    salary = Column(Integer, nullable=False)
    promotion_date = Column(DATE, nullable=False)

    owner = relationship("User", back_populates="items")
