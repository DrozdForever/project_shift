from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select, insert, update
from sqlalchemy.ext.asyncio import AsyncSession

from auth.base_config import fastapi_users
from operations.schemas import SalaryCreate, SalaryUpdate
from auth.models import User
from operations.models import Salary
from database import get_async_session

router = APIRouter(
    prefix="/salary",
    tags=["Salary"]
)


current_user = fastapi_users.current_user()
current_superuser = fastapi_users.current_user(active=True, superuser=True)


@router.get("/operations")
async def get_salary(
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_async_session)
):
    query = select(Salary).where(Salary.user_id == user.id)
    result = await session.execute(query)
    if len(result.scalars().all()) == 0:
        return [{"user_id": user.id, "salary": 45000, "promotion_date": "2025-05-20"}]
    result = await session.execute(query)
    return result.scalars().all()


@router.post("/operations")
async def add_salary(
        schema: SalaryCreate,
        session: AsyncSession = Depends(get_async_session),
        superuser: User = Depends(current_superuser)
):
    try:
        stmt = insert(Salary).values(**schema.dict())
        await session.execute(stmt)
        await session.commit()
        return {"status": "success"}
    except Exception:
        raise HTTPException(status_code=500, detail={
            "status": "error",
            "data": None,
            "details": None})


@router.put("/operations")
async def update_salary(
        schema: SalaryUpdate,
        session: AsyncSession = Depends(get_async_session),
        superuser: User = Depends(current_superuser)
):
    stmt = (
        update(Salary).
        where(Salary.user_id == schema.user_id).
        values(**schema.dict())
    )
    await session.execute(stmt)
    await session.commit()
    return {"status": "success"}

